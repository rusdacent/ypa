<?php

use app\models\LoadRecord;
use yii\db\Migration;

/**
 * Handles the creation of table `server_load`.
 */
class m191205_151242_create_server_load_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->getDb()->createCommand("CREATE TABLE `server_load` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `memory_percent` tinyint(3) unsigned NOT NULL,
  `memory_value` int(10) unsigned NOT NULL,
  `cpu_percent` tinyint(3) unsigned NOT NULL,
  `space_percent` tinyint(3) unsigned NOT NULL,
  `space_value` bigint(20) unsigned NOT NULL,
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `swap_percent` tinyint(3) unsigned NOT NULL,
  `swap_value` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `server_load_id_IDX` (`id`) USING BTREE
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='статистика по нагрзке на сервер привязанная к временной метке'")->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(LoadRecord::tableName());
    }
}
