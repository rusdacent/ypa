<?php

use yii\db\Migration;
use app\models\Task;

/**
 * Class m191226_124400_add_status_column_to_task
 */
class m191226_124400_add_status_column_to_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->getDb()
            ->createCommand("ALTER TABLE ".Task::tableName()
                ." ADD status ENUM('".Task::STATUS_APPROVED."', '".Task::STATUS_CREATED."') NOT NULL;")
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->getDb()
            ->createCommand("ALTER TABLE ".Task::tableName()." DROP COLUMN status")
            ->execute();
    }
}
