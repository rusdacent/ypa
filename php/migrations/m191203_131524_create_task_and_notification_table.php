<?php

use app\models\Notification;
use app\models\Task;
use yii\db\Migration;

class m191203_131524_create_task_and_notification_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->getDb()->createCommand("CREATE TABLE IF NOT EXISTS `".Task::tableName()."` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `id_parent_task` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `filename` varchar(255) COLLATE utf8_bin NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `filename_UNIQUE` (`filename`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `id_parent_task_INDEX` (`id_parent_task`),
  KEY `user_id` (`user_id`),
  KEY `creation_date` (`creation_date`),
  CONSTRAINT `tasks->user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;")->execute();
        $this->getDb()->createCommand("CREATE TABLE `".Notification::tableName()."` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT 'текст уведомления',
  `creation_date` datetime DEFAULT NULL,
  `extra` text COLLATE utf8_bin COMMENT 'дополнительное поле для помещения кастомных данных, которые возможно понадобятся позже',
  `task_id` int(11) unsigned NOT NULL COMMENT 'связь с задачей',
  `command` varchar(512) COLLATE utf8_bin DEFAULT NULL COMMENT 'Оригинальная команда, которая привела к выбросу уведомления',
  `type` int(11) DEFAULT '0' COMMENT 'тип уведомления (бага, RCE и прочее)',
  `level` int(11) DEFAULT '0' COMMENT 'уровень уведомления (некритичный, критический и прочее)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `task->notifications_idx` (`task_id`),
  CONSTRAINT `task->notifications` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;")->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(Notification::tableName());
        $this->dropTable(Task::tableName());
    }
}
