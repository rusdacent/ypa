<?php

namespace app\tests\unit\models\commands;

use app\models\commands\NmapCommand;

class NmapCommandTest extends BaseCommandTest
{
    /**
     * @var NmapCommand
     */
    protected $nmapCommand;

    public function getTestOutputFilename(): string
    {
        return 'nmap.txt';
    }

    public function setUp()
    {
        $this->nmapCommand = new NmapCommand();
        return parent::setUp();
    }

    public function tearDown()
    {
        unset($this->nmapCommand);
        parent::tearDown();
    }

    public function testPreExecute()
    {
        $this->nmapCommand->host = 'test.domain';
        $this->nmapCommand->preExecute();
        $this->assertAttributeEquals('nmap test.domain',
            'originalCommand',
            $this->nmapCommand);
    }

    public function testNmapParsing()
    {
        $publisher = new \app\models\AMQPPublisher(self::getAMQPConnection());
        /**
         * @var NmapCommand $tmpNmapCommand
         */
        $tmpNmapCommand = $this->make(
            get_class($this->nmapCommand),
            [
                'output' => $this->testOutputData,
                'addHeartbleederCommand' => function (string $domain, int $port = 0) {
                    return;
                },
                'addCmsCommand' => function (string $protocol, string $domain, int $port) {
                    return;
                },
                'addDirParserCommand' => function (string $protocol, string $domain) {
                    return;
                },
                'addHydraCommand' => function (
                    string $protocol,
                    string $address,
                    int $port,
                    bool $useProxy = false,
                    string $userList = '',
                    string $passList = ''
                ) {
                    return;
                },

            ]
        );
        $tmpNmapCommand->setPublisher($publisher);
        $tmpNmapCommand->host = '1.1.1.1';
        $tmpNmapCommand->domain = 'host.com';
        $tmpNmapCommand->postExecute();
        $httpServicesCount = $tmpNmapCommand->getHttpServicesCount();
        $httpsServicesCount = $tmpNmapCommand->getHttpsServicesCount();
        $isSshFound = $tmpNmapCommand->isSshFound();

        $this->assertEquals(true, $isSshFound);
        $this->assertEquals(1, $httpServicesCount);
        $this->assertEquals(1, $httpsServicesCount);
    }

    /**
     * @param string $outputFilename
     * @return NmapCommand
     * @throws \Exception
     */
    protected function getPublishedMessagesForMockedObject(string $outputFilename) : NmapCommand
    {
        $publisher = new \app\models\AMQPPublisher(self::getAMQPConnection());
        /**
         * @var NmapCommand $tmpNmapCommand
         */
        $tmpNmapCommand = $this->make(
            get_class($this->nmapCommand),
            [
                'output' => file_get_contents($this->getOutputFilenameFullPath($outputFilename)),
                'taskId' => 1,
            ]
        );
        $tmpNmapCommand->setPublisher($publisher);
        $tmpNmapCommand->host = '1.1.1.1';
        $tmpNmapCommand->domain = 'host.com';
        $tmpNmapCommand->postExecute();
        return $tmpNmapCommand;
    }

    public function testNmapAddHydraCommand()
    {
        $tmpNmapCommand = $this->getPublishedMessagesForMockedObject('nmapAddHydra.txt');
        $publishedMessages = $tmpNmapCommand->getPublishedMessages();
        $messageExpected = [
            'taskId' => 1,
            'domain' => 'host.com',
            'command' => 'app\\models\\commands\\HydraCommand',
            'extra' => [
                'url' => 'ssh://host.com:22',
                'useProxy' => false,
            ],
        ];
        $data = json_decode($publishedMessages[0], true);
        $this->assertEquals($messageExpected, $data);
    }

    public function testHttpsServiceReport()
    {
        $tmpNmapCommand = $this->getPublishedMessagesForMockedObject('nmapHttpsService.txt');
        $publishedMessages = $tmpNmapCommand->getPublishedMessages();
        $messageExpectedHeartbleeder = [
            'taskId' => 1,
            'domain' => 'host.com',
            'command' => 'app\\models\\commands\\HeartbleederCommand',
            'extra' => [
                'port' => 443,
            ],
        ];
        $messageExpectedCms = [
            'taskId' => 1,
            'domain' => 'host.com',
            'command' => 'app\\models\\commands\\PhpmyadminCommand',
            'extra' => [
                'protocol' => 'https',
                'domain' => 'host.com',
                'path' => '',
                'port' => 443,
            ],
        ];
        $messageExpectedDirParser = [
            'taskId' => 1,
            'domain' => 'host.com',
            'command' => 'app\\models\\commands\\DirParserCommand',
            'extra' => [
                'url' => 'https://host.com',
            ],
        ];
        $data_addHeartbleederCommand = json_decode($publishedMessages[0], true);
        $data_addCmsCommand = json_decode($publishedMessages[1], true);
        $data_addDirParserCommand = json_decode($publishedMessages[2], true);
        $this->assertEquals($messageExpectedHeartbleeder, $data_addHeartbleederCommand);
        $this->assertEquals($messageExpectedCms, $data_addCmsCommand);
        $this->assertEquals($messageExpectedDirParser, $data_addDirParserCommand);
        $this->assertEquals(1, $tmpNmapCommand->getHttpsServicesCount());
    }

    public function testHttpServiceReport()
    {
        $tmpNmapCommand = $this->getPublishedMessagesForMockedObject('nmapHttpService.txt');
        $publishedMessages = $tmpNmapCommand->getPublishedMessages();
        $messageExpectedCms = [
            'taskId' => 1,
            'domain' => 'host.com',
            'command' => 'app\\models\\commands\\PhpmyadminCommand',
            'extra' => [
                'protocol' => 'http',
                'domain' => 'host.com',
                'path' => '',
                'port' => 80,
            ],
        ];
        $messageExpectedDirParser = [
            'taskId' => 1,
            'domain' => 'host.com',
            'command' => 'app\\models\\commands\\DirParserCommand',
            'extra' => [
                'url' => 'http://host.com',
            ],
        ];
        $data_addCmsCommand = json_decode($publishedMessages[0], true);
        $data_addDirParserCommand = json_decode($publishedMessages[1], true);
        $this->assertEquals($messageExpectedCms, $data_addCmsCommand);
        $this->assertEquals($messageExpectedDirParser, $data_addDirParserCommand);
        $this->assertEquals(1, $tmpNmapCommand->getHttpServicesCount());
    }

}