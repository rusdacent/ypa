<?php

namespace app\tests\unit\models\commands;

use app\models\commands\SubfinderCommand;

class SubfinderCommandTest extends BaseCommandTest
{
    /**
     * @var app\models\commands\SubfinderCommand
     */
    protected $subfinderCommand;

    public function getTestOutputFilename(): string
    {
        return 'subfinder.txt';
    }

    public function setUp()
    {
        $this->subfinderCommand = new SubfinderCommand();
    }

    public function tearDown()
    {
        unset($this->subfinderCommand);
    }

    public function testPreExecute()
    {
        $this->subfinderCommand->domain = 'test.domain';
        $this->subfinderCommand->preExecute();
        $this->assertAttributeEquals('subfinder -d test.domain',
            'originalCommand',
            $this->subfinderCommand);
    }

    public function testPostExecute()
    {
        $publisher = new \app\models\AMQPPublisher(self::getAMQPConnection());
        /**
         * @var SubfinderCommand $subfinderCommand
         */
        $subfinderCommand = $this->make(
            get_class($this->subfinderCommand),
            [
                'output' => $this->testOutputData,
                'domain' => 'host.com',
                'taskId' => 1,
            ]
        );
        $subfinderCommand->setPublisher($publisher);
        $subfinderCommand->postExecute();
        $this->assertEquals(4, $subfinderCommand->getFoundDomainsCount());
    }
    
}