<?php

return [
    'host' => 'rabbit',
    'port' => 5672,
    'vhost' => '/ypa',
    'user' => getenv('RABBITMQ_DEFAULT_USER'),
    'password' => getenv('RABBITMQ_DEFAULT_PASS'),
    'queue' => 'task',
];