<?php

declare(strict_types=1);

namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "notification".
 *
 * @property integer $id
 * @property string $text
 * @property string $creation_date
 * @property string $extra
 * @property integer $task_id
 * @property string $command
 * @property integer $type
 * @property integer $level
 * @property integer $taken_by_user_id
 * @property string $short_info
 *
 * @property Task $task
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName() : string
    {
        return 'notification';
    }

    /**
     * @inheritdoc
     */
    public function rules() : array
    {
        return [
            [['creation_date'], 'safe'],
            [['extra'], 'string'],
            [['task_id'], 'required'],
            [['task_id', 'type', 'level', 'taken_by_user_id'], 'integer'],
            [['short_info'], 'string', 'max' => 255],
            [['text'], 'string', 'max' => 4000],
            [['command'], 'string', 'max' => 512],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() : array
    {
        return [
            'id' => 'ID',
            'text' => 'текст уведомления',
            'creation_date' => 'Creation Date',
            'extra' => 'дополнительное поле для помещения кастомных данных, которые возможно понадобятся позже',
            'task_id' => 'связь с задачей',
            'command' => 'Оригинальная команда, которая привела к выбросу уведомления',
            'type' => 'тип уведомления (бага, RCE и прочее)',
            'level' => 'уровень уведомления (некритичный, критический и прочее)',
            'taken_by_user_id' => 'Взят пользователем',
            'short_info' => 'Краткая информация',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask() : ActiveQuery
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }
    
    public function getErrorsAsString() : string
    {
        $string = '';
        $errors = $this->getErrors();
        foreach ($errors as $attributeName => &$attributeErrors) {
            $string .= "\nAttribute - '$attributeName':\n" . implode("\n", $attributeErrors);
        }
        return $string;
    }
    
    /**
     * Создано ли уведомление задачей администратора
     * @return bool
     */
    public function isCreatedByAdmin() : bool
    {
        return intval(Task::find()
            ->select(['user.id'])
            ->leftJoin('user', 'user.id = task.user_id')
            ->where(['task.id' => $this->task_id])->count()) === 1;       
    }
}
