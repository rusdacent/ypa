<?php
namespace app\models;

use Yii;
use yii\web\UploadedFile;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use app\models\auth\User;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property string $name
 * @property integer $id_parent_task
 * @property string $creation_date
 * @property string $filename
 * @property string $status
 * @property integer $user_id
 */
class Task extends \yii\db\ActiveRecord
{
    
    const STATUS_CREATED = 'created';
    const STATUS_APPROVED = 'approved';

    const UPLOAD_FOLDER = 'uploads';

    public $domains;

    /**
     * @inheritdoc
     */
    public static function tableName() : string
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules() : array
    {
        return [
            [['name', 'filename'], 'required'],
            [['id_parent_task', 'user_id'], 'integer'],
            [['creation_date'], 'safe'],
            [['name', 'filename', 'status'], 'string', 'max' => 255],
            [['filename'], 'unique'],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() : array
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            //'id_parent_task' => 'Id Parent Task',
            //'creation_date' => 'Creation Date',
            //'filename' => 'Filename',
        ];
    }

    /**
     * @param yii\web\UploadedFile $uf 
     * @return string
     */
    protected function uploadFile(UploadedFile $uf) : string
    {
        if (!($uf instanceof UploadedFile)) {
            throw new \Exception('$uf is not instance of UploadedFile');
        }

        $basename = stripslashes($uf->getBaseName()) . '-' . md5(time());
        $ext = 'txt';
        $filePath = Yii::getAlias('@webroot') . "/../" . Task::UPLOAD_FOLDER;
        $filePath = str_replace('web/../', '', $filePath);
        if (!file_exists($filePath)) {
            mkdir($filePath, 0755);
        }
        $filePath .= "/$basename.$ext";

        $error = $uf->saveAs($filePath);

        return $filePath;
    }

    /**
     * 
     * @return array
     */
    public function readDomains() : array
    {
        $content = file_get_contents($this->filename);
        $lines = explode("\n", str_replace("\r", '', $content));
        array_unique($lines);
        foreach ($lines as $key => &$line) {
            if (empty($line)) {
                unset($lines[$key]);
            }
        }
        return $lines;
    }

    /**
     * @param boolean $runValidation
     * @param array $attributeNames
     * @return bool
     * @throws \Exception
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $this->status = Task::STATUS_CREATED;
        $this->creation_date = date_create('now', new \DateTimeZone(\Yii::$app->timeZone))->format('Y-m-d H:i:s');
        $tmpDomainsFile = UploadedFile::getInstance($this, 'domains');
        if ($tmpDomainsFile !== null)
        {
            $filPath = $this->uploadFile($tmpDomainsFile);
            $this->filename = $filPath;
        }      
        $userId = User::getCurrentUserId();
        $this->user_id = $userId > 0 ? $userId : null;
        $saveResult = parent::save();
        if (!$saveResult) {
            throw new \Exception($this->getErrorsAsString());
        }

        return $saveResult;
    }
    
    public function getErrorsAsString() : string
    {
        $string = '';
        $errors = $this->getErrors();
        foreach ($errors as $attributeName => &$attributeErrors) {
            $string .= "\nAttribute - '$attributeName':\n" . implode("\n", $attributeErrors);
        }
        return $string;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        $this->pushToQueue('task');        
        parent::afterSave($insert, $changedAttributes);
    }
    
    /**
     * 
     * @param string $queue
     * @return integer 
     */
    public function pushToQueue(string $queue) : int
    {
        $rabbitConnectionData = Yii::$app->params['rabbit'];
  
        $exchange = commands\AbstractCommand::RABBIT_EXCHANGE_DEFAULT;
        $connection = new AMQPStreamConnection(
            $rabbitConnectionData['host'], 
            $rabbitConnectionData['port'], 
            $rabbitConnectionData['user'], 
            $rabbitConnectionData['password'],
            $rabbitConnectionData['vhost']);
        $channel = $connection->channel();
        //$channel->queue_declare($queue, false, true, false, false);
        $publisher = new AMQPPublisher($connection);
        $domains = $this->readDomains();
        $domainsCount = 0;
        foreach ($domains as $key => $domain) 
        { 
            $message = $publisher->buildMessage(
                $this->id,
                $domain,
                commands\PingCommand::class,
                ['previousCommand' => null]
            );
            $publisher->publishMessage($message, $exchange, AMQPPublisher::ROUTING_KEY_HOST_ADDED);
            $domainsCount++;
        }
        
        $channel->close();
        $connection->close();
        
        return $domainsCount;
    }
    
    /**
     * 
     * @param string $domain
     * @param string $commandName
     * @param array $extra
     * @return AMQPMessage
     */
    protected function buildRabbitMessage(string $domain, string $commandName, array $extra = null) : AMQPMessage
    {
        $msg = [
            'taskId' => $this->id,
            'domain' => $domain,
            'command' => $commandName === null ? 'host' : $commandName,
            'extra' => $extra,
        ];
        
        $message = new AMQPMessage(json_encode($msg), 
                [
                    'content_type' => 'text/plain', 
                    'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
                ]);
        return $message;
    }
    
    public function getUser() : ActiveQueryInterface
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
