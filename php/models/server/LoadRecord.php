<?php


namespace app\models\server;


use app\models\server\Load;
use yii\db\ActiveRecord;

/**
 * Запись сбора статистики в единицу времени
 * @package app\models
 * @property id
 * @property integer memory_percent
 * @property integer memory_value
 * @property integer cpu_percent
 * @property integer space_percent
 * @property integer space_value
 * @property string creation_date
 * @property integer swap_percent
 * @property integer swap_value
 */
class LoadRecord extends ActiveRecord
{
    
    const TYPE_MEMORY = 'memory';
    const TYPE_CPU = 'cpu';
    const TYPE_SPACE = 'space';
    
    /**
     * @var Load  $l;
     */
    protected $l;

    /**
     * @inheritdoc
     */
    public function rules() : array
    {
        return [
            [
                [
                    'memory_percent',
                    'memory_value',
                    'cpu_percent',
                    'space_percent',
                    'space_value',
                    'swap_percent',
                    'swap_value'
                ],
                'integer'
            ],
            [
                [
                    'memory_percent',
                    'memory_value',
                    'cpu_percent',
                    'space_percent',
                    'space_value',
                    'swap_percent',
                    'swap_value'
                ],
                'required'
            ],
            [['creation_date'], 'safe'],
        ];
    }

    public function setData(Load $l)
    {
        $this->l = $l;
        $memoryLoad = $l->parseMemoryLoad();
        $this->memory_percent = $memoryLoad[Load::RESULT_INDEX_USED_PERCENT];
        $this->memory_value = $memoryLoad[Load::RESULT_INDEX_USED_VALUE];
        $this->swap_percent = $memoryLoad[Load::RESULT_INDEX_MEMORY_SWAP_PERCENT];
        $this->swap_value = $memoryLoad[Load::RESULT_INDEX_MEMORY_SWAP_VALUE];
        $this->cpu_percent = $l->parseCpuLoad();
        $space = $l->parseStorageInfo();
        $this->space_percent = $space[Load::RESULT_INDEX_USED_PERCENT];
        $this->space_value = $space[Load::RESULT_INDEX_USED_VALUE];
    }

    public function toString(): string
    {
        return "Memory: {$this->memory_value}Mb({$this->memory_percent}%)
Free space: {$this->space_value}Mb({$this->space_percent}%)
Cpu load: {$this->cpu_percent}%
Date: $this->creation_date
";
    }

    /**
     * @inheritdoc
     */
    public static function tableName() : string
    {
        return 'server_load';
    }
    
    public static function isAllowedType(string $type): bool
    {
        return $type === self::TYPE_CPU 
                || $type === self::TYPE_MEMORY 
                || $type === self::TYPE_SPACE;
    }

    /**
     * @param string $type
     * @param array $additionalColumns
     * @return array
     */
    public static function getColumnsByType(string $type, array $additionalColumns = []): array
    {
        switch ($type)
        {
            case self::TYPE_MEMORY:
                $columns = ['memory_percent', 'memory_value'];
                break;
            case self::TYPE_CPU:
                $columns = ['cpu_percent'];
                break;
            case self::TYPE_SPACE:
                $columns = ['space_percent', 'space_value'];
                break;
            default:
                return [];
        }

        if (count($additionalColumns) > 0)
        {
            $columns = array_merge($columns, $additionalColumns);
        }
        return $columns;
    }

    
    /**
     * Запрос записей для отрисовки графиков
     * x -> время
     * y -> величина в процентах
     * текст у точки -> величина (поле *_value)
     * @param string $date
     * @param string $type
     * @param int $limit
     * @return array
     */
    public static function findByDateAndType(string $date, string $type, int $limit, array $additionalColumns = []) : array
    {
        $columns = self::getColumnsByType($type, $additionalColumns);
        if ($columns === [])
        {
            return [];
        }
        $dateFieldName = 'creation_date';
        $query = LoadRecord::find()->select($columns)
                ->where(['<=',$dateFieldName, $date])
                ->limit($limit)
                ->orderBy([$dateFieldName => SORT_DESC])->asArray();
        
        return $query->all();
    }
    
}