<?php

declare(strict_types=1);

namespace app\models\templates;

use app\models\PageSource;

interface ISimilar
{
    /**
     * Сравнение шаблонов друг с другом на предмет похожести
     * @param app\models\PageSource $template
     * @return boolean результат проверки
     */
    public function looksLike(PageSource $template) : bool ;
}
