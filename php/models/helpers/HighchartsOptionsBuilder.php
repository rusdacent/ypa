<?php

declare (strict_types=1);

namespace app\models\helpers;

use app\models\server\LoadRecord;
use yii\db\Exception;
use yii\web\JsExpression;

class HighchartsOptionsBuilder {

    /**
     *
     * @param string $type
     * @param int $limit
     * @return array
     * @throws Exception
     */
    public static function buildSpline(string $type, int $limit, string $title): array {
        $firstData = json_encode(self::getSplineStartData($type, $limit));
        return [
            'chart' => [
                'type' => 'spline',
                'animation' => 'Highcharts.svg',
                'marginRight' => 10,
                'events' => [
                    'load' => new JsExpression("function () {
                var series = this.series[0];
                setInterval(function () {
                    var type = '{$type}';
                    getServerLoadData(series, 'now', type, 1, function(data){
                        if (data.length === 0)
                        {
                            return;
                        }
                        var point = [(new Date()).getTime(), parseInt(data[0][type+'_percent'])];
                        series.name = type+'_value' in data[0] ? data[0][type+'_value'] + 'Mb' : data[0][type+'_percent'] + '%';
                        series.addPoint(point, true, true); 
                    });                    
                }, 1000)}")
                ],
            ],
            'time' => ['useUTC' => false],
            'title' => ['text' => $title],
            'xAxis' => [
                'type' => 'datetime',
                'tickPixelInterval' => 100,
            ],
            'yAxis' => [
                'title' => ['text' => 'Percent usage'],
                'plotLines' => [
                    [
                        'value' => 0,
                        'width' => 100,
                        'color' => '#808080',
                    ]
                ],
            ],
            'tooltip' => [
                'headerFormat' => '<b>{series.name}</b><br/>',
                'pointFormat' => '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}',
            ],
            'legend' => ['enabled' => false],
            'exporting' => ['enabled' => false],
            'series' => [[
            'name' => '',
            'data' => new JsExpression("(function () {
                var data = JSON.parse('{$firstData}');
                return data;
        }())")
                ]],
        ];
    }

    /**
     * @param string $type
     * @param int $limit
     * @return array
     * @throws Exception
     */
    protected static function getSplineStartData(string $type, int $limit) : array
    {
        if (!LoadRecord::isAllowedType($type))
        {
            return [];
        }
        
        $typeValue = $type === LoadRecord::TYPE_CPU ? '' : "`{$type}_value`, ";      
        $date = date_create("-1 seconds", new \DateTimeZone(\Yii::$app->timeZone))
            ->format('Y-m-d H:i:s');
        $query = "SELECT * FROM (
                        SELECT 
                            `{$type}_percent` AS `y`, 
                            $typeValue 
                            UNIX_TIMESTAMP(`creation_date`)*1000 AS `x`, 
                            creation_date 
                        FROM `server_load` 
                        WHERE `creation_date` <= :date 
                        ORDER BY `creation_date` DESC LIMIT :limit
                ) a ORDER BY creation_date";
        $data = \Yii::$app->getDb()
                ->createCommand($query)
                ->bindValues([
                    ':date' => $date, 
                    ':limit' => $limit,
                    ])
                ->query()
                ->readAll();
        foreach ($data as &$record)
        {
            $record['x'] = (int)$record['x'];
            $record['y'] = (int)$record['y'];
        }
        return $data;
    }

}
