<?php

namespace app\models;

use PhpAmqpLib\Connection\AMQPStreamConnection;


class AMQPConnectionBuilder
{
    public static function build(
        string $host,
        string $port,
        string $user,
        string $password,
        string $vhost) : AMQPStreamConnection
    {
        $connection = new AMQPStreamConnection(
            $host,
            $port,
            $user,
            $password,
            $vhost,
            $insist = false,
            $login_method = 'AMQPLAIN',
            $login_response = null,
            $locale = 'en_US',
            $connection_timeout = 60*50,
            $read_write_timeout = 60*50,
            $context = null,
            $keepalive = false,
            $heartbeat = 30*50);
        return $connection;
    }
}