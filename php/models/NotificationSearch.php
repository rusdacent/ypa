<?php

declare(strict_types=1);

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Notification;
use app\models\auth\User;

/**
 * NotificationSearch represents the model behind the search form about `app\models\Notification`.
 */
class NotificationSearch extends Notification
{
    /**
     * @inheritdoc
     */
    public function rules() : array
    {
        return [
            [['id', 'task_id', 'type', 'level'], 'integer'],
            [['text', 'creation_date', 'extra', 'command'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() : array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params) : ActiveDataProvider
    {
        $query = Notification::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'creation_date' => $this->creation_date,
            'task_id' => $this->task_id,
            'type' => $this->type,
            'level' => $this->level,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'extra', $this->extra])
            ->andFilterWhere(['like', 'command', $this->command]);
                   
        return $dataProvider;
    }
    
    /**
     * Поиск уведомлений текущего пользователя
     * Взятые им из уведомлений админа
     * или созданные задачами  текущего пользователя
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchForCurrentUserOnly(array $params) : ActiveDataProvider
    {
        $dataProvider = $this->search($params);
        $query = $dataProvider->query;
        
        $query->andFilterWhere(['taken_by_user_id' => User::getCurrentUserId(),]);        
        $query->join('LEFT JOIN', 'task', 'task.id = notification.task_id')
                ->orFilterWhere(['task.user_id' => User::getCurrentUserId()]);  
        
        return $dataProvider;        
    }
    
    public function searchCreatedByAdmin(array $params) : ActiveDataProvider
    {
        $dataProvider = $this->search($params);
        /**
         * @var ActiveQuery $query
         */
        $query = $dataProvider->query;
        $null = new yii\db\Expression('NULL');
        $query->join('LEFT JOIN', 'task', 'task.id = notification.task_id')
            ->join('LEFT JOIN', 'user', 'task.user_id = user.id')
            ->andFilterWhere(['user.is_admin' => 1])
            ->andFilterWhere(['is', 'taken_by_user_id',  $null]);
        
        return $dataProvider;
       
    }
}
