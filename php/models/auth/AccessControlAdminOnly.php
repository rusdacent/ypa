<?php

namespace app\models\auth;
use yii\filters\AccessControl;

class AccessControlAdminOnly extends AccessControl {
    
    /**
     * 
     * @param Action $action
     * @return bool
     */
    public function beforeAction($action) 
    {
        return User::currentUserIsAdmin();
    }
}
