<?php

declare(strict_types=1);

namespace app\models\auth;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Class User
 * @package app\models\auth
 * @property integer $id
 * @property string $name;
 * @property string $password;
 * @property string $auth_key;
 * @property string $token;
 * @property boolean $disabled;
 * @property boolean $is_admin;
 * @property string $creation_date;
 * @property string $update_date;
 * @property string $last_login;
 */
class User extends ActiveRecord implements IdentityInterface
{
    
    const LOGIN_AS_SESSION_KEY = 'loginAs';
    const ADMIN_ID_SESSION_KEY = 'adminId';
    
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @param int|string $id
     * @return User|IdentityInterface|null
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public function rules() : array
    {
        return [
            [['name', 'password', 'token', 'auth_key'], 'string', 'max' => 50],
            [['name'], 'unique'],
            [['id'], 'integer'],
            [['disabled', 'is_admin'], 'boolean'],
            [['creation_date', 'update_date', 'last_login'], 'safe'],
        ];
    }


    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['token' => $token]);
    }


    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return bool
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function disable(bool $disabled)
    {
        $this->disabled = $disabled;
    }

    public function validatePassword($password)
    {
        return \Yii::$app->getSecurity()->validatePassword($password, $this->getPasswordHash());
    }

    public function beforeSave($insert)
    {
        $this->password = \Yii::$app->getSecurity()->generatePasswordHash($this->password);
        return parent::beforeSave($insert);
    }

    public function getPasswordHash()
    {
        return $this->password;
    }

    public static function findByUsername($username)
    {
        return self::findOne(['name' => $username]);
    }

    /**
     * проверка прав админа в системе
     * @return bool
     */
    public function isAdmin() : bool
    {
        return $this->is_admin;
    }
    
    public static function currentUserIsAdmin() : bool
    {
        /**
         * @var User $user
         */
        $user = \Yii::$app->user->getIdentity();
        return $user !== null && $user->isAdmin();
    }
    
    /**
     * Возвращает текущего пользователя или выброс исключения, если гость
     * @return \app\models\auth\User
     * @throws Exception
     */
    public static function getCurrentUser() : User
    {
        $user =  \Yii::$app->user->getIdentity();
        if ($user === null)
        {
            throw new Exception('Current user is guest');
        }
        return $user;
    }
    
    /**
     * Возвращает id текущего пользователя
     * 0 если пользователь не найден
     * @return int
     */
    public static function getCurrentUserId() : int
    {
        /**
         * @var User $user
         */
        $user = self::getCurrentUser();
        return $user->id;
    }

}