<?php

declare(strict_types=1);

namespace app\models\payloads;

use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use app\models\Notification;

abstract class AbstractPayload
{   
    
    protected $queue;
    
    /**
     *
     * @var AMQPStreamConnection;
     */
    protected $connection;
    
    /**
     * 
     * @param AMQPStreamConnection $connection
     */
    public function setConnection(AMQPStreamConnection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * 
     * @param string $queue
     */
    public function __construct(string $queue)
    {
        $this->queue = $queue;
    }

    /**
     * 
     * @param AMQPMessage $message
     */
    abstract public function execute(AMQPMessage $message);

    /**
     * 
     * @param \stdClass $data
     */
    abstract public function afterExecute(\stdClass $data);
    
    /**
     * 
     * @param AMQPMessage $message
     * @param \stdClass $data
     */
    protected function sendSuccess(AMQPMessage $message, \stdClass $data = null)
    {
        $this->afterExecute($data);
        if (!empty($message->delivery_info) && array_key_exists('channel', $message->delivery_info))
        {
            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
        }
    }
    
    /**
     * 
     * @param \Exception $ex
     */
    protected function printException(\Exception $ex)
    {
        echo $ex->getMessage();
        echo "\n";
        echo $ex->getTraceAsString();
        echo "\n";
    }
}
