<?php

declare(strict_types=1);

namespace app\models;

use yii\base\Model;
use app\models\payloads\AbstractPayload;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class AMQPWorker extends Model
{
    const CONFIG_INDEX_HOST = 'vhost',
        CONFIG_INDEX_QUEUE = 'queue';
        
    protected $vHost = null;
    protected $queue = null;
    protected $tag;

    /**
     * @var AbstractPayload
     */
    protected $payload;
    
    protected $channel;
    protected $connection;
    
    public function __construct(AbstractPayload $payload)
    {
        $this->payload = $payload;
        $this->tag = rand(0, 100);
    }

    /**
     * @param AMQPStreamConnection $connection
     */
    public function connect(AMQPStreamConnection $connection)
    {
        $this->connection = $connection;
        //подключение к очереди
        $this->connection->set_close_on_destruct(false);
        $this->channel = $this->connection->channel();
        $this->channel->basic_qos(0, 1, false);        
        $this->payload->setConnection($this->connection);
    }
    
    /**
     * 
     * @param string $queue
     */
    public function listen(string $queue = '')
    {
        $this->queue = $queue;
        $this->channel->basic_consume($queue, $this->getTag(), 
                false, 
                false, 
                false, 
                false, 
                [$this->payload, commands\AbstractCommand::RABBIT_QUEUE_DEFAULT]);
        while (count($this->channel->callbacks)) 
        {
            $this->channel->wait();
        }
    }
    
    public function getTag() : int
    {
        return $this->tag;
    }
    
    ///**
    // * @param \PhpAmqpLib\Channel\AMQPChannel $channel
    // * @param \PhpAmqpLib\Connection\AbstractConnection $connection
    // */
    public function shutdown() 
    {
        if ($this->channel !== null) {
            $this->channel->close();
        }
        if ($this->connection !== null) {
            $this->connection->close();
        }
    }
}
