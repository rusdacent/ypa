<?php

declare(strict_types=1);


namespace app\models\commands;

/**
 *
 * враппер для утилиты subfinder
 * @see https://github.com/subfinder/subfinder
 * @package app\models\commands
 */
class SubfinderCommand extends AbstractCommand
{

    /**
     * @var array
     */
    protected $domains = [];

    /**
     * @var string
     */
    public $domain;

    public function preExecute()
    {
        $this->setCommand("subfinder -d $this->domain");
    }

    public function postExecute()
    {
        $lines = explode("\n", $this->output);
        $rDomain = ".$this->domain";
        $lineIsDomain = false;
        foreach ($lines as &$line) {
            $line = str_replace("\r", "", $line);
            if ($lineIsDomain && !empty($line))
            {
                if ($line == $rDomain)
                {
                    array_push($this->domains, $this->domain);
                }
                else
                {
                    array_push($this->domains, trim($line));
                }

                continue;
            }

            if (strpos($line, "Unique subdomains found for {$this->domain}"))
            {
                $lineIsDomain = true;
                continue;
            }
        }

        if (count($this->domains))
        {
            $this->pushDomainsToPing();
        }
    }

    /**
     * Количество найденных доменов
     *
     * @return int
     */
    public function getFoundDomainsCount() : int
    {
        return count($this->domains);
    }

    protected function pushDomainsToPing()
    {
        foreach ($this->domains as $domain)
        {
            $message = $this->buildMessage(
                $this->taskId,
                $domain,
                PingCommand::class,
                ['previousCommand' => self::class]
            );
            $this->publishMessage($message,
                AbstractCommand::RABBIT_EXCHANGE_DEFAULT,
                \app\models\AMQPPublisher::ROUTING_KEY_DISCOVER_TOOL);
        }
    }



    public static function getCommandName() : string
    {
        return 'subfinder';
    }
}