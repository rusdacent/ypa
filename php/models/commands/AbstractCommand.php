<?php

declare(strict_types=1);

namespace app\models\commands;

use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use app\models\Notification;
use Yii;
use app\models\Task;
use app\models\AMQPPublisher;

abstract class AbstractCommand
{
    const RABBIT_QUEUE_DEFAULT = 'execute';
    const RABBIT_EXCHANGE_DEFAULT = 'task';

    protected static $RABBIT_POSSIBLE_VHOSTS = [
        '/ypa',
        '/ypa-high',
        '/ypa-medium',
        '/ypa-low',
    ];

    protected $output;
    private $originalCommand;

    protected $executionResult;

    protected $taskId;

    /**
     *
     * @var \app\models\AMQPPublisher
     */
    private $publisher;

    /**
     * @var array
     */
    protected $publishedMessages;

    /**
     *
     * @param AMQPStreamConnection $connection
     */
    public function __construct(AMQPStreamConnection $connection = null)
    {
        if ($connection !== null) {
            $this->publisher = new AMQPPublisher($connection);
        }
    }

    /**
     * установка в свойство $publishedMessages последнего отправленного в очередь сообщения
     * @param AMQPMessage $message
     */
    private function setLastPublishedMessage(AMQPMessage $message)
    {
        $this->publishedMessages []= $message->body;
    }

    /**
     * @return array
     */
    public function getPublishedMessages() : array
    {
        return $this->publishedMessages;
    }

    /**
     * @param int $taskId
     * @param string $domain
     * @param string $commandName
     * @param array $extra
     * @return AMQPMessage
     */
    protected function buildMessage(int $taskId, string $domain, string $commandName, array $extra = []) : AMQPMessage
    {
        return $this->publisher->buildMessage($taskId, $domain, $commandName, $extra);
    }

    /**
     * Обертка над методом \app\models\AMQPPublisher->publishMessage перед вызовом которого в свойство $lastPublishedMessage
     * устанавливается значение $message
     * @param $message
     * @param $exchange
     * @param string $routingKey
     */
    protected function publishMessage($message, $exchange, $routingKey = '')
    {
        $this->setLastPublishedMessage($message);
        if (defined('YII_ENV') && YII_ENV === 'test')
        {
            return;
        }
        return $this->publisher->publishMessage($message, $exchange, $routingKey);
    }

    /**
     * @param AMQPPublisher $publisher
     */
    public function setPublisher(AMQPPublisher $publisher)
    {
        $this->publisher = $publisher;
    }

    /**
     * инициализация свойств из данных в сообщении из очереди
     * @param stdClass $msgBody
     */
    public function initParameters(\stdClass $msgBody)
    {
        // опциональный параметр, которого может не быть в команде
        if (property_exists($msgBody, 'domain'))
        {
            $this->domain = $msgBody->domain;
        }

        // обязательный параметр, любая команда
        // должна быть привязана к задаче
        $this->taskId = $msgBody->taskId;
    }
    
    public function run() : string
    {
        $this->preExecute();        
        $this->output = shell_exec($this->originalCommand);        
        $this->postExecute();
        
        $this->executionResult = $this->output !== null;
        
        return $this->output;
        
    }
    
    public function isSuccess() : bool
    {
        return $this->executionResult;
    }
    
    abstract public function preExecute();
    
    abstract public function postExecute();
    
    abstract public static function getCommandName();
    
    public function setCommand($command)
    {
        $this->originalCommand = $command;
    }
    
    public function getCommand() : string
    {
        return $this->originalCommand;
    }
    
    public function debugPrint($string)
    {
        if (defined('YII_DEBUG') && YII_DEBUG == true)
        {
            echo $string;
            echo "\n";
        }
    }
    
    /**
     * 
     * @param integer $type
     * @param integer $level
     * @param array $extra
     * @return int
     * @throws \Exception
     */
    protected function saveNotificationToDB(int $type, int $level, array $extra = null) : int
    {
        $n = new Notification();
        $n->task_id = $this->taskId;
        $n->type = $type;
        $n->command = $this->getCommand();
        $n->level = $level;
        $n->extra = $extra !== null ? implode("\n", $extra) : '';
        $n->creation_date = date_create('now', new \DateTimeZone(\Yii::$app->timeZone))->format('Y-m-d H:i:s');
        
        if ($n->validate() && $n->save())
        {
            return $n->id;
        }
        
        throw new \Exception($n->getErrorsAsString());

    }

    /**
     * @param string $line
     * @param string $substr
     * @return bool
     */
    protected function lineBeginsAt(string $line, string $substr) : bool
    {
        return strpos($line, $substr) === 0;
    }

    /**
     * @param string $substr
     * @return bool
     */
    protected function outputContains(string $substr) : bool
    {
        return strpos($this->output, $substr) !== false;
    }
    
    /**
     * 
     * @param string $line
     * @param array $dataArray
     * @return boolean
     */
    protected function lineContains(string $line, array $dataArray) : bool
    {
        foreach ($dataArray as $key => &$value) {
            if (strpos($line, $value) !== false)
            {
                return true;
            }
        }
        
        return false; 
    }
}
