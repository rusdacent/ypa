<?php

declare(strict_types=1);

namespace app\models\commands;

class HostCommand extends AbstractCommand
{
    /**
     *
     * @var string 
     */
    public $domain;

    protected $publishedAddressesCount = 0;
    
    public function postExecute()
    {
        $searchString = "$this->domain has address ";
        $lines = explode("\n", $this->output);
        foreach ($lines as &$line) {
            if (strpos($line, $searchString) === false)
            {
                continue;
            }

            $ip = trim(str_replace($searchString, '', $line));
            $this->debugPrint("IP FOUND: $ip");
            $message = $this->buildMessage(
                $this->taskId,
                $this->domain,
                NmapCommand::class,
                ['host' => $ip]
            );
            $this->publishMessage($message,
                AbstractCommand::RABBIT_EXCHANGE_DEFAULT,
                \app\models\AMQPPublisher::ROUTING_KEY_DISCOVER_TOOL);
            $this->publishedAddressesCount++;
        }
    }

    public function getPublishedAddressesCount()
    {
        return $this->publishedAddressesCount;
    }

    public function preExecute()
    {
        $this->setCommand("host {$this->domain}");
    }

    public static function getCommandName()
    {
        return 'host';
    }
}
