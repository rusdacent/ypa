<?php

declare(strict_types=1);

namespace app\models\commands;

class NmapCommand extends AbstractCommand
{
    const PROTOCOL_HTTP = 'http';
    const PROTOCOL_HTTPS = 'https';

    /**
     *
     * @var string 
     */
    public $host;

    /**
     * @var string
     */
    public $domain;

    /**
     * @var int количество найденных http сервисов
     */
    protected $httpServicesCount = 0;

    /**
     * @var int количество найденных https сервисов
     */
    protected $httpsServicesCount = 0;

    /**
     * @var bool найден ли ssh среди доступных сервисов
     */
    protected $isSshFound = false;

    /**
     * @var int количество открытых портов у хоста
     */
    protected $openedPortsCount = 0;

    /**
     * @return int
     */
    public function getOpenedPortsCount(): int
    {
        return $this->openedPortsCount;
    }

    /**
     * @return int
     */
    public function getHttpServicesCount() : int
    {
        return $this->httpServicesCount;
    }

    /**
     * @return int
     */
    public function getHttpsServicesCount() : int
    {
        return $this->httpsServicesCount;
    }

    public function isSshFound() : bool
    {
        return $this->isSshFound;
    }
    
    public function postExecute()
    {
        $lines = explode("\n", $this->output);
        foreach ($lines as &$line) {
            $port = intval($line);
            if (!$port)
            {
                continue;
            }

            $this->openedPortsCount++;
            if (strpos($line, '80/tcp') !== false || strpos($line, '443/tcp') !== false)
            {
                if (strpos($line, self::PROTOCOL_HTTPS))
                {
                    $this->httpsServicesCount++;
                    $this->debugPrint("on $this->host found new HTTPS server");
                    $this->addHeartbleederCommand($this->domain, $port);
                    $this->addCmsCommand(self::PROTOCOL_HTTPS, $this->domain, $port);
                    $this->addDirParserCommand(self::PROTOCOL_HTTPS, $this->domain);
                }
                else if (strpos($line, self::PROTOCOL_HTTP))
                {
                    $this->httpServicesCount++;
                    $this->debugPrint("on $this->host found new HTTP server");
                    $this->addCmsCommand(self::PROTOCOL_HTTP, $this->domain, $port);
                    $this->addDirParserCommand(self::PROTOCOL_HTTP, $this->domain);
                }
            }
            else if ((strpos($line, 'open') !== false || strpos($line, 'filtered') !== false)
                && strpos($line, 'ssh') !== false)
            {
                $this->isSshFound = true;
                $this->addHydraCommand('ssh', $this->domain, $port);
            }

        }
    }

    /**
     * Отправка в очередь задания для брута сервиса через thc hydra
     * @param string $protocol
     * @param string $address
     * @param integer $port
     * @param bool $useProxy использовать proxychains при работе
     * @param string $userList полный путь к листу юзеров
     * @param string $passList полный путь к листу с паролями
     * @see https://github.com/vanhauser-thc/thc-hydra
     */
    protected function addHydraCommand(string $protocol, string $address, int $port, bool $useProxy = false, string $userList = '', string $passList = '')
    {
        $url = "$protocol://$address:$port";
        $extra = [
            'url' => $url,
            'useProxy' => $useProxy,
        ];
        if (!empty($userList))
        {
            $extra['userList'] = $userList;
        }
        if (!empty($passList))
        {
            $extra['passList'] = $passList;
        }

        $message = $this->buildMessage(
            $this->taskId,
            $this->domain,
            HydraCommand::class,
            $extra
        );
        $this->publishMessage($message, self::RABBIT_EXCHANGE_DEFAULT);

    }

    /**
     * @param string $domain
     * @param integer $port
     */
    protected function addHeartbleederCommand(string $domain, int $port = 0)
    {
        $message = $this->buildMessage(
            $this->taskId,
            $domain,
            HeartbleederCommand::class,
            $port !== 0 ? ['port' => $port] : 0
            );
        $this->publishMessage($message, self::RABBIT_EXCHANGE_DEFAULT);
    }

    /**
     * @param string $protocol
     * @param string $domain
     * @param integer $port
     */
    protected function addCmsCommand(string $protocol, string $domain, int $port)
    {
        $extra = [
            'protocol' => $protocol,
            'domain' => $domain,
            'path' => '',
            'port' => $port
        ];
        $message = $this->buildMessage(
            $this->taskId,
            $domain,
            PhpmyadminCommand::class,
            $extra
        );
        $this->publishMessage($message, self::RABBIT_EXCHANGE_DEFAULT);
    }

    /**
     * @param string $protocol
     * @param string $domain
     */
    protected function addDirParserCommand(string $protocol, string $domain)
    {
        $extra = [
            'url' => "$protocol://$domain",
        ];
        $message = $this->buildMessage(
            $this->taskId,
            $domain,
            DirParserCommand::class,
            $extra
        );
        $this->publishMessage($message, self::RABBIT_EXCHANGE_DEFAULT);
    }

    /**
     * @param stdClass $msgBody
     * @inheritdoc
     */
    public function initParameters(\stdClass $msgBody)
    {
        parent::initParameters($msgBody);
        $this->host = $msgBody->host;
    }

    /**
     * @param boolean $isHttps
     * @param string $domain
     */
    protected function pushPhpmyadminChecker(bool $isHttps, string $domain)
    {
        $extra = ['isHttps' => $isHttps];
        $message = $this->buildMessage(
                $this->taskId, 
                $domain, 
                PhpmyadminCommand::class,
                $extra
            );
        $this->publishMessage($message, self::RABBIT_EXCHANGE_DEFAULT);
    }

    public function preExecute()
    {
        $this->setCommand("nmap $this->host");
    }

    public static function getCommandName()
    {
        return 'nmap';
    }
}
