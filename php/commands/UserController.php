<?php

declare(strict_types=1);

namespace app\commands;

use app\models\User;
use Yii;
use yii\console\Controller;

class UserController extends Controller
{
    /**
     * @param string $name
     * @param string $password
     * @param bool $isAdmin
     */
    public function actionAdd(string $name, string $password, bool $isAdmin)
    {
        $user = new \app\models\auth\User();
        $user->name = $name;
        $user->password = $password;
        $user->disabled = false;
        $user->is_admin = $isAdmin;
        if (!$user->save())
        {
            echo "errors on ".__FUNCTION__."\n";
            $errors = $user->getErrors();
            foreach ($errors as $error)
            {
                echo var_export($errors);
                echo "\n";
            }
        }
        else
        {
            echo "Added user $name (id = $user->id)\n";
        }
    }
}