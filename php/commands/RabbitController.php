<?php

declare(strict_types=1);

namespace app\commands;
use app\models\AMQPConnectionBuilder;
use yii\base\Module;
use yii\console\Controller;
use app\models\AMQPWorker;
use app\models\payloads\TaskProcessor;
use app\models\payloads\NotificationProcessor;

class RabbitController extends Controller
{
    protected $w;
    
    public function __construct(string $id, Module $module, array $config = array())
    {
        error_reporting(E_ALL & ~E_NOTICE);
        ini_set('display_errors', 1);
        ini_set("memory_limit", "64M");
        $this->w = null;
        parent::__construct($id, $module, $config);
    }
    
    public function actionNotification()
    {
        $name = $this->getQueueName(__FUNCTION__);
        $this->loop($name);
    }
    
    /**
     * Выполение задачи из очереди
     * @param string $queue
     */
    public function actionTask(string $queue = null)
    {
        $name = $this->getQueueName(__FUNCTION__);
        $this->loop($name, $queue);
    }
    
    /**
     * Получение имени очереди для выполняемой задачи
     * @param string $functionName
     * @return string
     */
    protected function getQueueName(string $functionName)
    {
        return str_replace('action', '', $functionName);        
    }
    
    /**
     * Цикл обработки сообщений воркером
     * @param string $payloadName Название класса из неймспейса app\models\payloads
     * инстанс класса AbstractPayload
     * @see app\models\payloads\AbstractPayload
     * @param string $queue название очереди (если не используется, то берется из конфига)
     */
    protected function loop(string $payloadName, string $queue = null)
    {
        $classname = "app\\models\\payloads\\{$payloadName}Processor";

        $rabbitConnectionData = Yii::$app->params['rabbit'];
        $connection = AMQPConnectionBuilder::build(
            $rabbitConnectionData['host'],
            $rabbitConnectionData['port'],
            $rabbitConnectionData['user'],
            $rabbitConnectionData['password'],
            $rabbitConnectionData[AMQPWorker::CONFIG_INDEX_HOST]
        );

        $payload = new $classname(lcfirst($payloadName));
        $this->w = new AMQPWorker($payload);
        
        $this->w->connect($connection);
        if (empty($queue))
        {
            $queue = \Yii::$app->params['rabbit']['queue'];
        }
    
        $this->w->listen($queue);
    }
    
    public function __destruct()
    {
        if ($this->w instanceof AMQPWorker)
        {
            $this->w->shutdown();
        }        
    }
}
