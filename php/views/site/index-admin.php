<?php

use miloschuman\highcharts\Highcharts;
use app\models\helpers\HighchartsOptionsBuilder;
use app\models\server\LoadRecord;
use app\models\server\Process;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

$command = new \app\models\commands\PsCommand();
                $command->run();
                $provider = new ArrayDataProvider([
                    'allModels' => $command->getRunningProcesses(),
                    'sort' => [
                        //'attributes' => ['id', 'username', 'email'],
                    ],
                    'pagination' => [
                        'pageSize' => 30,
                    ],
                ]);

/* @var $this yii\web\View */

$this->title = '';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Admin index</h1>
    </div>

    <div class="body-content">
        <div class="row">
            <?= Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => HighchartsOptionsBuilder::buildSpline(
                    LoadRecord::TYPE_MEMORY,
                    19,
                    'MEMORY LOAD'
                ),
            ]); ?>
        </div>
        <div class="row">
            <?= Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => HighchartsOptionsBuilder::buildSpline(
                    LoadRecord::TYPE_CPU,
                    19,
                    'CPU LOAD'
                ),
            ]); ?>
        </div>
        <div class="row">
            <?php
                /*echo GridView::widget([
                    'dataProvider' => $provider,
                    'columns' => Process::getGridViewColumnsArray(),
                ]);*/
            ?>
        </div>

    </div>
</div>
<script>
    
    /**
     * 
     * @param {string} date
     * @param {string} type
     * @param {integer} limit
     * @param {function} callback
     * @returns {undefined}
     */
    function getServerLoadData(series, date, type, limit, callback)
    {
        $.ajax({
            url: '<?= \Yii::$app->getUrlManager()->createUrl('server-load/get') ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                date: 'now', 
                type: type, 
                limit: limit,
                'csrf-param': typeof variable !== 'yii' ? yii.getCsrfParam() : '',
                'csrf-token': typeof variable !== 'yii' ? yii.getCsrfToken() : ''
            },
            success: function (data) {
                //console.log('success', data[0]);
                callback(data);
            }
        });
    }
</script>
