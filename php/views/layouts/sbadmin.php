<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\SbAppAsset;

SbAppAsset::register($this);
$urlManager = \Yii::$app->getUrlManager();
/**
 * @var $identity \app\models\auth\User
 */
$identity = Yii::$app->user->getIdentity();
$loggedIn = $identity instanceof \app\models\auth\User;
$isAdmin = $loggedIn && $identity->isAdmin();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div id="wrapper">

    <?php if ($loggedIn) : ?>
        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= \Yii::$app->getHomeUrl(); ?>">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-3">YPA panel</div>
            </a>
            <!-- Divider -->
            <hr class="sidebar-divider my-0">
            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="<?= \Yii::$app->getHomeUrl(); ?>">
                    <i class="fas fa-home"></i>
                    <span>Home</span></a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider">
            <!-- Heading -->
            <div class="sidebar-heading">
                Information
            </div>
            <!-- Nav Item - Charts -->
            <li class="nav-item">
                <a class="nav-link" href="<?= $urlManager->createUrl('task/index'); ?>">
                    <i class="fas fa-tasks"></i>
                    <span>Tasks</span></a>
            </li>
            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-envelope"></i>
                    <span>Notifications</span>
                </a>
                <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <!--<h6 class="collapse-header"></h6>-->
                        <a class="collapse-item" href="<?= $urlManager->createUrl('notification/index'); ?>">All</a>
                        <a class="collapse-item" href="<?= $urlManager->createUrl('notification/general'); ?>">General</a>
                    </div>
                </div>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider">
            <!-- Heading -->
            <div class="sidebar-heading">
                Rest
            </div>
            <!-- Nav Item - Tables -->
            <li class="nav-item">
                <a class="nav-link" href="<?= $urlManager->createUrl('site/about'); ?>">
                    <i class="fas fa-question"></i>
                    <span>About</span></a>
                <?php if ($isAdmin) : ?>
                    <a class="nav-link" href="<?= $urlManager->createUrl('user/index'); ?>">
                        <i class="fas fa-user-cog"></i>
                        <span>Users</span></a>
                <?php endif; ?>
                <?= Html::beginForm(['/site/logout'], 'post', ['class' => '']) ?>
                <?= Html::beginTag('button', ['class' => 'nav-link nav-menu-button', 'type' => 'submit']) ?>
                <i class="fas fa-sign-out-alt"></i>
                <span>Logout</span></a>
                <?= Html::endTag('button') ?>
                <?= Html::endForm() ?>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">
            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
    <?php endif; ?>

    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            <div class="container-fluid">
                <?php if($loggedIn) : ?>
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800"><?= Html::encode($this->title); ?></h1>
                        <!--<a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>-->
                    </div>
                <?php endif; ?>
                <?= $content ?>
            </div>
        </div>
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright &copy; Your Website 2019</span>
                </div>
            </div>
        </footer>
    </div>
</div>

<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
