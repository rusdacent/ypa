<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\auth\User;

/* @var $this yii\web\View */
/* @var $model app\models\Notification */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Notifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notification-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
            if ($model->taken_by_user_id === User::getCurrentUserId())
            {
                echo Html::a(
                        'Free', [
                        'free', 'id' => $model->id], 
                        ['class' => 'btn btn-primary']
                    );
            }
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'text',
            'creation_date',
            'extra:ntext',
            'command',
            'type',
            'level',
        ],
    ]) ?>

</div>
