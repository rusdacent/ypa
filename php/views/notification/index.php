<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use app\models\auth\User;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NotificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notification-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Notification', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'short_info',
            'creation_date',
            'type',
            'level',
            [
                'attribute' => 'task_id',
                'format' => 'raw',
                'value' => function ($model) {
                    $task = $model->getTask()->one();
                    return Html::a(Html::encode($task->name), 
                    Url::to(['task/view', 'id' => $task->id]));
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'free' => function ($url, $model, $key) {
                        if ($model->taken_by_user_id !== User::getCurrentUserId())
                        {
                            return '';
                        }                       
                        $data = [
                            'notification/free', 
                            'id' => $model->id,
                            ];
                        $icon = 'fa-file-export';
                        return Html::a ( '<span class="fa '.$icon.'" aria-hidden="true"></span> ', $data);
                    },
                ],
                'template' => '{view} {free}'
            ],
            
        ],
    ]); ?>
</div>
