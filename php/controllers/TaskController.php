<?php

declare(strict_types=1);

namespace app\controllers;

use Yii;
use app\models\Task;
use app\models\TaskSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use app\models\auth\User;
use yii\filters\AccessControl;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends BaseController
{
    /**
     *
     * @var string 
     */
    protected static $nonAdminMesage = 'текущий пользователь не является администратором';

    /**
     * @inheritdoc
     */
    public function behaviors() : array
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'approve', 'push'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'approve', 'push'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex() : string
    {
        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Task model.
     * @param integer $id
     * @return mixed
     */
    public function actionView(int $id) : string
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
            'domains' => $model->readDomains(),
            'domainsCount' => 0,
        ]);
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Task();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $response = new Response();
            $response = $this->render('create', [
                'model' => $model,
                //'domains' => $model->readDomains(),
            ]);
            return $response;
        }
    }
    
    /**
     * 
     * @param int $id
     */
    public function actionApprove(int $id)
    {
        if (!User::currentUserIsAdmin())
        {
            \Yii::error(__METHOD__.": ".self::$nonAdminMesage);
            return $this->redirect(['view', 'id' => $model->id]);
        } 
        
        return $this->redirect(['view', 'id' => $id]);
    }
    
    /**
     * 
     * @param integer $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPush(int $id) : string
    {
        $model = $this->findModel($id);
        if (!User::currentUserIsAdmin())
        {
            \Yii::error(__METHOD__.": ".self::$nonAdminMesage);
            return $this->redirect(['view', 'id' => $model->id]);
        }
        
        if ($model->status !== Task::STATUS_APPROVED)
        {
            throw new \Exception('Task must be in "approved" status');
        }
        if (!$model)
        {
            throw new NotFoundHttpException('Not found task with id '.$id);
        }
        
        $domainsCount = $model->pushToQueue('task');
        return $this->render('view', [
            'model' => $model,
            'domains' => $model->readDomains(),
            'domainsCount' => $domainsCount,
        ]);      
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id) : Task
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
