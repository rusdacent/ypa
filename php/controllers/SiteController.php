<?php

declare(strict_types=1);

namespace app\controllers;

use app\models\auth\User;
use app\models\TaskSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use yii\web\Response;

class SiteController extends BaseController
{
    public function behaviors() : array
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions() : array
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() : string
    {
        $tmp = shell_exec("/home/test/p0f-mtu/p0f-client /home/test/p0f-socket {$_SERVER['REMOTE_ADDR']}");
        /**
         * @var User $user
         */
        $user = Yii::$app->user->getIdentity();
        if ($user === null) {
            return $this->render('index-guest');
        }

        if ($user->isAdmin())
        {
            return $this->render('index-admin');
        }

        return $this->render('index-user');
    }

    public function actionLogin() : Response
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        $response = new Response();
        $response->content = $this->renderPartial('login', [
            'model' => $model,
        ]);
        return $response;
    }

    public function actionLogout() : Response
    {    
        $sessionAdminId = \Yii::$app->getSession()->get(User::ADMIN_ID_SESSION_KEY);
        Yii::$app->user->logout();       
        if (intval($sessionAdminId) > 0)
        {
            $model = new LoginForm();
            if (!$model->loginAs($sessionAdminId))
            {
                \Yii::error(__METHOD__.": не удалось вернуться в сессию админа после выхода из пользователя.");
            }
            else
            {
                \Yii::$app->getSession()->set(User::ADMIN_ID_SESSION_KEY, null);
            }
        }

        return $this->goHome();
    }

    public function actionAbout() : string
    {
        return $this->render('about');
    }
    
    public function actionInfo() : string
    {
        return $this->render('info');
    }

    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        $this->layout = 'sbadmin';
        if ($exception instanceof \Exception)
        {
            return $this->render('error', ['exception' => $exception]);
        }
        return $this->goHome();
    }
}
