<?php


namespace app\assets;

use yii\web\AssetBundle;

class SbAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
        'css/sb-admin-2.css'
    ];
    public $js = [
        'js/bootstrap.bundle.min.js',
        'js/sb-admin-2.js',
    ];
    public $depends = [
        'rmrevin\yii\fontawesome\NpmFreeAssetBundle',
        'uran1980\yii\assets\jQueryEssential\JqueryEasingAsset',
        'yii\web\YiiAsset',
        //'app\assets\BootstrapAssetBundle',
    ];
}