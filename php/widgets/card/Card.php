<?php

declare(strict_types=1);

namespace app\widgets\card;

use app\widgets\progressbar\ProgressBar;
use yii\base\Widget;

class Card extends Widget {

    const TYPE_PRIMARY = 'primary';
    const TYPE_SUCCESS = 'success';
    const TYPE_INFO = 'info';
    const TYPE_WARNING = 'warning';

    public $cardType;

    public $icon;

    public $progressBar;

    public $header;

    public $text;

    public function run() : string
    {
        $progressBarView = '';
        if ($this->progressBar instanceof ProgressBar)
        {
            $this->progressBar->type = $this->cardType;
            $progressBarView = $this->progressBar->run();
        }

        return $this->render('main', [
            'cardType' => $this->cardType,
            'icon' => $this->icon,
            'progressBarView' => $progressBarView,
            'header' => $this->header,
            'text' => $this->text,
        ]);
    }

}
