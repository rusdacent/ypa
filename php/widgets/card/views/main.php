<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $header string */
/* @var $text string */
/* @var $icon string */
/* @var $cardType string */
/* @var $progressBarView string */

?>
<div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-<?= Html::encode($cardType); ?> shadow h-100 py-2">
        <div class="card-body">
            <?php if (mb_strlen($progressBarView) > 0): ?>
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-<?= Html::encode($cardType); ?> text-uppercase mb-1"><?= Html::encode($header); ?></div>
                        <div class="row no-gutters align-items-center">
                            <div class="col-auto">
                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?= Html::encode($text); ?></div>
                            </div>
                            <div class="col">
                                <?= $progressBarView ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas <?= Html::encode($icon); ?> fa-2x text-gray-300"></i>
                    </div>
                </div>
            <?php else: ?>
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-<?= Html::encode($cardType); ?> text-uppercase mb-1"><?= Html::encode($header); ?></div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= Html::encode($text); ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas <?= Html::encode($icon); ?> fa-2x text-gray-300"></i>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

