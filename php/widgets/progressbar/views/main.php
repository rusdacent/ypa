<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $value string */
/* @var $max string */
/* @var $min string */
/* @var $type string */

?>
<div class="progress progress-sm mr-2">
    <div class="progress-bar bg-<?= Html::encode($type); ?>"
         role="progressbar"
         style="width: <?= Html::encode($value); ?>%"
         aria-valuenow="<?= Html::encode($value); ?>"
         aria-valuemin="<?= Html::encode($min); ?>"
         aria-valuemax="<?= Html::encode($max); ?>">
    </div>
</div>