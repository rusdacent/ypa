from abc import ABC, abstractmethod
from aiohttp import web
import subprocess


class AbstractWrapper(ABC):

    @classmethod
    @abstractmethod
    def get_command(cls) -> str:
        pass

    @classmethod
    @abstractmethod
    def validate_input(cls, request):
        pass

    @abstractmethod
    def get_route(self) -> str:
        pass

    @classmethod
    async def handle_request(cls, request) -> web.Response:
        cls.validate_input(request)
        result, is_error = cls.execute(cls.get_command())
        if is_error:
            return web.Response(text=result.decode("utf-8"), status=500)
        return web.Response(text=result.decode("utf-8"))

    @classmethod
    def execute(cls, command: str) -> str:
        process = subprocess.Popen(command,
                                   shell=True,
                                   executable='/bin/bash',
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        if len(stderr) > 0:
            return stderr, True
        return stdout, False


class WpscanWrapper(AbstractWrapper):

    @classmethod
    def validate_input(cls, request):
        cls.domain = request.match_info.get('domain', "Anonymous")

    def get_route(self) -> str:
        return '/wpscan/{domain}'

    @property
    def domain(self) -> str:
        return

    @classmethod
    def get_command(cls) -> str:
        return f'docker run -i --rm wpscanteam/wpscan --url https://{cls.domain}/ --enumerate u'
