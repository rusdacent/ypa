from aiohttp import web
from container_wrappers.wrappers import WpscanWrapper

wpscan = WpscanWrapper()

app = web.Application()
app.add_routes([web.get(wpscan.get_route(), wpscan.handle_request)])

if __name__ == '__main__':
    web.run_app(app, host='172.17.0.1', port=5555)
